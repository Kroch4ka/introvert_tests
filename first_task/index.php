<?php

declare(strict_types=1);

require_once realpath(__DIR__ . '/controllers/budget_controller.php');


if (isset($clientData) && isset($totalBudgetClients)):?>
    <table border="2" style="border-collapse: collapse;">
        <caption>Таблица бюджетов</caption>
        <tr>
            <th>ID клиента</th>
            <th>Название клиента</th>
            <th>Сумма успешных сделок</th>
        </tr>
        <?php foreach($clientData as $client):?>
            <tr>
                <td><?=$client['id']?></td>
                <td><?=$client['name']?></td>
                <td><?=$client['totalBudget']?></td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td>Общий бюджет всех клиентов: </td>
            <td><?=$totalBudgetClients?></td>
        </tr>
    </table>
<?php endif;?>
