<?php

require_once realpath(__DIR__ . '../../helpers/budgetTools.php');

use function BudgetTools\{getClients,getFormatClientData, getTotalBudgetFromClients};

if (isset($_GET['date_from']) && isset($_GET['date_to'])) {
    $from = $_GET['date_from'];
    $to = $_GET['date_to'];
    $assocClients = getClients();
    $clientData = getFormatClientData($from, $to, $assocClients);
    $totalBudgetClients = getTotalBudgetFromClients($from, $to, $assocClients);
} else {
    throw new Exception('Не заданы корректные GET параметры');
}