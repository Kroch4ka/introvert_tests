<?php

namespace BudgetTools;

require_once realpath(__DIR__ . './commonTools.php');

use function CommonTools\apiConnect;

use Exception;

function getTotalBudgetFromSuccessLeads(int $from, int $to, string $apiKey): int
{
    $api = apiConnect($apiKey);
    $leadResponse = $api->lead->getAll(null, 142, null, null, 0, 0);
    try {
        if (!$leadResponse['code']) {
            throw new Exception(`Возникла ошибка, код ошибки: ` . $leadResponse['code']);
        }
        $leads = $leadResponse['result'];
        $successLeads = array_filter($leads, function ($lead) use ($from, $to) {
            return $from <= $lead['date_close'] && $to >= $lead['date_close'];
        });
        $total = array_reduce($successLeads, function ($accum, $lead) {
            return $accum + (int) $lead['price'];
        });
        return $total;
    } catch (Exception $err) {
        echo $err;
        exit();
    }
}

function getClients()
{
    return [
        [
            'id' => 1,
            'name' => 'intrdev',
            'api' => '23bc075b710da43f0ffb50ff9e889aed'
        ],
        [
            'id' => 2,
            'name' => 'artedegrass0',
            'api' => '',
        ],

    ];
}

function getTotalBudgetFromClients(int $from, int $to, array $assocClients): int
{

    $apiKeys = array_map(function ($client) {
        if (array_key_exists('api', $client)) {
            if ($client['api']) {
                return $client['api'];
            } else {
                throw new Exception('Передано пустое значение API ключа');
            }
        }
        throw new Exception('Некорректный ключ в переданном массиве: ');
    }, $assocClients);
    return array_reduce($apiKeys, function ($accum, $clientApi) use ($from, $to) {
        return $accum + getTotalBudgetFromSuccessLeads($from, $to, $clientApi);
    });
}


function getFormatClientData(int $from, int $to, $assocClients): array
{
    $resultData = array_map(function ($client) use ($from, $to) {
        if (array_key_exists('api', $client)) {
            if ($client['api']) {
                $client['totalBudget'] = getTotalBudgetFromSuccessLeads($from, $to, $client['api']);
                return $client;
            } else {
                throw new Exception('Передано пустое значение API ключа');
            }
        }
        throw new Exception('Некорректный ключ в переданном массиве: ');
    }, $assocClients);
    return $resultData;
}
