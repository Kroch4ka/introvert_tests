<?php

namespace CommonTools;

require realpath(__DIR__ . '../../../vendor/autoload.php');


function apiConnect(string $apiKey): \Introvert\ApiClient
{
    \Introvert\Configuration::getDefaultConfiguration()->setApiKey('key', $apiKey);
    $api = new \Introvert\ApiClient();
    return $api;
}