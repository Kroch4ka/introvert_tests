document.addEventListener('DOMContentLoaded', () => {


    googlingFunctionWidget = function() {
        var widget = this;
        this.code = null;


        function renderNewTip() {
            const newTip = document.createElement('div');
            newTip.classList.add('tips-item', 'js-tips-item', 'js-cf-actions-item');
            newTip.dataset.type = 'googling';
            const innerSpan = document.createElement('a');
            innerSpan.classList.add('tips-icon', 'icon', 'icon-inline', 'icon-eye');
            newTip.appendChild(innerSpan);
            newTip.append('Гуглить');
            return newTip;
        }

        function setClickHandlerForAllElements(container, handler) {
            container.forEach((elem) => {
                elem.addEventListener('click', () => {
                    handler(elem.value);
                });
            });
        }

        function handlerForOpenTabs(inputValue) {
            const urls = [`http://letmegooglethat.com/?q=${inputValue}`, `https://yandex.ru/search/?text=${inputValue}`];
            urls.forEach((url) => window.open(url));
        }


        // вызывается один раз при инициализации виджета, в этой функции мы вешаем события на $(document)
        this.bind_actions = function() {

            const emailDropDown = document.querySelectorAll('div[data-pei-code="email"] .tips__inner');
            const phoneDropDown = document.querySelectorAll('div[data-pei-code="phone"] .tips__inner');
            setClickHandlerForAllElements(emailDropDown, () => {
                handlerForOpenTabs();
            });
            setClickHandlerForAllElements(phoneDropDown, () => {
                handlerForOpenTabs();
            });
        };

        // вызывается каждый раз при переходе на страницу
        this.render = function() {
            const emailDropDown = document.querySelectorAll('div[data-pei-code="email"] .tips__inner');
            const phoneDropDown = document.querySelectorAll('div[data-pei-code="phone"] .tips__inner');
            emailDropDown.forEach((elem) => {
                elem.appendChild(renderNewTip());
            });
            phoneDropDown.forEach((elem) => {
                elem.appendChild(renderNewTip());
            });
        };

        // вызывается один раз при инициализации виджета, в этой функции мы загружаем нужные данные, стили и.т.п
        this.init = function() {

        };

        // метод загрузчик, не изменяется
        this.bootstrap = function(code) {
            widget.code = code;
            // если frontend_status не задан, то считаем что виджет выключен
            // var status = yadroFunctions.getSettings(code).frontend_status;
            var status = 1;

            if (status) {
                widget.init();
                widget.render();
                widget.bind_actions();
                $(document).on('widgets:load', function() {
                    widget.render();
                });
            }
        }
    };

    // создание экземпляра виджета и регистрация в системных переменных Yadra
    // widget-name - ИД и widgetNameIntr - уникальные названия виджета
    yadroWidget.widgets['googlingFunctionWidget'] = new googlingFunctionWidget();
    yadroWidget.widgets['googlingFunctionWidget'].bootstrap('googlingFunctionWidget');

});