<?php

require_once realpath(__DIR__ . './helpers/lead_tools.php');

use function LeadTools\getCountReservations;

header('Content-Type: application/json');

echo json_encode(getCountReservations(944613, '23bc075b710da43f0ffb50ff9e889aed', 9585813, 20715778));