document.addEventListener('DOMContentLoaded', () => {
    // Устанавливаем региональные языковые настройки
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: 'Предыдущий',
        nextText: 'Следующий',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);




    // Асинхронно делаем GET запрос
    async function getResponse(url) {
        const requestResponse = await fetch(url);
        if (requestResponse.ok) {
            const responseBody = await requestResponse.json();
            return responseBody;
        } else {
            throw new Error('Сетевая ошибка, код: ' + requestResponse.status);
        }
    }


    // Обрабатываем метки времени, фильтруя по максимальному количеству забронированных мест
    function timeStampProcessing(timeStampData) {
        const maxBookAmount = 5;
        const timeStamps = Object.keys(timeStampData);
        const busyTimeStamps = timeStamps.filter((timestamp) => {
            return timeStampData[timestamp] > maxBookAmount;
        });
        return getDateFromTimestamp(busyTimeStamps);
    }

    // получаем корректный формат для обработки datepicker-ом
    function getDateFromTimestamp(timestampArray) {
        return timestampArray.map((timeStamp) => {
            const miliTimeStamp = +timeStamp * 1000;
            return $.datepicker.formatDate("yy-mm-dd", new Date(miliTimeStamp));
        });
    }

    // Делаем асинхронный запрос и выводим обработанный календарь
    getResponse('./ajax.php').then((response) => {
        const busyDates = timeStampProcessing(response);
        $("#datepicker").datepicker({
            minDate: 0,
            maxDate: '30d',
            beforeShowDay: (date) => {
                const formatDate = $.datepicker.formatDate("yy-mm-dd", date);
                return [!busyDates.includes(formatDate), ''];
            }
        });
    }).catch((err) => {
        console.log(err);
    });
});