<?php

declare(strict_types=1);

namespace LeadTools;

use Exception;

use function CommonTools\apiConnect;

define('API_KEY', '23bc075b710da43f0ffb50ff9e889aed');

require_once realpath(__DIR__ . '../../../vendor/autoload.php');
require_once realpath(__DIR__ . '../../../first_task/helpers/commonTools.php');


/**
 * @param int $fieldId
 * @param string $apiKey
 * @param int ...$statusLeads
 * 
 * @return [type]
 * 
 * Возвращаем отображение метки времени и количества бронированных мест
 */
function getCountReservations(int $fieldId, string $apiKey, int ...$statusLeads): array
{
    $api = apiConnect($apiKey);
    try {
        $leadResponse = $api->lead->getAll(null, $statusLeads, null, null, 0, 0);
    } catch (Exception $err) {
        echo 'Возникла ошибка: ' . $err->getMessage() . PHP_EOL;
    }
    $leadsWithoutEmptyCustomFields = getLeadsWithoutEmptyCustomFields($leadResponse);
    $customFields = getCustomFields($leadsWithoutEmptyCustomFields);
    $customFieldsWithTargetField = getCustomFieldWithTargetId($customFields, $fieldId);
    $customFieldsWithTimeValue = getCustomFieldsTimeValue($customFieldsWithTargetField);
    $customFieldsWithTargetTimeValue = getFieldsTargetTimeValues($customFieldsWithTimeValue);
    $resultTimeStampArray = setAssocTimeArray($customFieldsWithTargetTimeValue);
    return $resultTimeStampArray;
}


/**
 * @param array $customFields
 * 
 * @return array
 * 
 * Возвращаем значения времени из массива значений доп поля, лежащие в определённом промежутке
 */
function getFieldsTargetTimeValues(array $customFields): array {
    return array_filter($customFields, function($time) {
        return time() <= $time && $time < time() + 2.592e6;
    });
}


/**
 * @param array $leads
 * 
 * @return array
 * 
 * Возвращаем доп поля
 */
function getCustomFields(array $leads): array {
    return array_map(function ($lead) {
        return $lead['custom_fields'];
    }, $leads);
}


/**
 * @param array $leadResponse
 * 
 * @return array
 * 
 * Возвращаем сделки без пустых доп полей
 */
function getLeadsWithoutEmptyCustomFields(array $leadResponse): array {
    return array_filter($leadResponse['result'], function ($lead) {
        return $lead['custom_fields'];
    });
}


/**
 * @param array $customFields
 * @param int $fieldId
 * 
 * @return array
 * 
 * Возвращаем доп поля соответствуют целевому id поля
 */
function getCustomFieldWithTargetId(array $customFields, int $fieldId): array
{
    return array_filter(array_map(function ($field) use ($fieldId) {
        foreach ($field as $miniField) {
            foreach ($miniField as $targetField) {
                if ($targetField === $fieldId) {
                    return $miniField;
                }
            }
        }
    }, $customFields), function ($field) {
        return $field !== null;
    });
}


/**
 * @param array $customFields
 * 
 * @return array
 * 
 * Возвращаем временные значения из доп полей
 */
function getCustomFieldsTimeValue(array $customFields): array {
    return array_map(function($field){
        [$year, $month, $day] = explode('-', explode(' ', $field['values'][0]['value'])[0]);
        return mktime(0, 0, 0, (int) $month, (int) $day, (int) $year);
    }, $customFields);
}



/**
 * @param array $timeArray
 * 
 * @return array
 * 
 * Возвращаем количество временных меток в виде ассоциативного массива
 */
function setAssocTimeArray(array $timeArray): array {
    $resultArray = [];
    foreach ($timeArray as $timeStamp) {
        if (array_key_exists($timeStamp, $resultArray)) {
            ++$resultArray[$timeStamp];
        } else {
            $resultArray[$timeStamp] = 1;
        }
    }
    return $resultArray;
}

