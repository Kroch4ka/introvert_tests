document.addEventListener('DOMContentLoaded', () => {
    changeColorTextPipeLine = function() {
        var widget = this;
        this.code = null;


        // вызывается один раз при инициализации виджета, в этой функции мы вешаем события на $(document)
        this.bind_actions = function() {
            //пример $(document).on('click', 'selector', function(){});
        };

        // вызывается каждый раз при переходе на страницу
        this.render = function() {
            const pipilineColor = document.querySelector('.pipeline_status:nth-child(4) .pipeline_status__head_line').style.backgroundColor;
            const targetSpan = document.querySelector('.pipeline_status:nth-child(4) .block-selectable');
            targetSpan.style.color = pipilineColor;
        };

        // вызывается один раз при инициализации виджета, в этой функции мы загружаем нужные данные, стили и.т.п
        this.init = function() {

        };

        // метод загрузчик, не изменяется
        this.bootstrap = function(code) {
            widget.code = code;
            // если frontend_status не задан, то считаем что виджет выключен
            // var status = yadroFunctions.getSettings(code).frontend_status;
            var status = 1;

            if (status) {
                widget.init();
                widget.render();
                widget.bind_actions();
                $(document).on('widgets:load', function() {
                    widget.render();
                });
            }
        }
    };

    // создание экземпляра виджета и регистрация в системных переменных Yadra
    // widget-name - ИД и widgetNameIntr - уникальные названия виджета
    yadroWidget.widgets['changeColorTextPipeLine'] = new changeColorTextPipeLine();
    yadroWidget.widgets['changeColorTextPipeLine'].bootstrap('changeColorTextPipeLine');

});